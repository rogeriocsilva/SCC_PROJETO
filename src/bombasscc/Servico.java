/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bombasscc;


import java.util.*;

// Classe que representa um servico com uma fila de espera associada
public class Servico {
    private int estado; // Variavel que regista o estado do servico: 0 - livre; 1 - ocupado
    private int atendidos; // Numero de clientes atendidos ate ao momento
    private double temp_ult, soma_temp_esp, soma_temp_serv; // Variaveis para calculos estatisticos
    //tipo 1 bomba, 2 caixa
    private int tipo;
    // Medias das distribuicoes de chegadas e de atendimento no servico
    private double media_cheg;
    private double media_serv;
    private int natendedores;
    private double tempo;
    TransServicos transserv;
    private Vector<Cliente> fila; // Fila de espera do servico
    private Simulador s; // Referencia para o simulador a que pertence o servico

    // Construtor
    
    Servico(Simulador s, double media_cheg, double media_serv, double tempo, int tipo, int natendedores) {
        this.media_cheg = media_cheg;
        this.media_serv = media_serv;
        this.tempo = tempo;
        this.tipo = tipo;
        this.s = s;
        this.natendedores = natendedores;
        
        fila = new Vector<Cliente>(); // Cria fila de espera
        estado = 0; // Livre
        temp_ult = s.getInstante(); // Tempo que passou desde o ultimo evento. Neste caso 0, porque a simulacao ainda nao comecou.
        atendidos = 0;  // Inicializacao de variaveis
        soma_temp_esp = 0;
        soma_temp_serv = 0;
    }
    
    // Metodo que insere cliente (c) no servico
    public void insereServico(Cliente c) {
        if (estado < natendedores) { // Se servico livre,
            estado++;     // fica ocupado e
            // agenda saida do cliente c para daqui a s.getMedia_serv() instantes
            if (tipo == 1)
                s.insereEvento(new TransServicos(s.getInstante() + Aleatorio.normal(getMedia_cheg(),getMedia_serv(),tipo)[0],s,c,this));
            else if (tipo == 2)
                s.insereEvento(new TransServicos(s.getInstante() + Aleatorio.normal(getMedia_cheg(), getMedia_serv(),tipo)[0],s,c,this));
            else if (tipo == 3){
                s.insereEvento(new Saida(s.getInstante() + Aleatorio.normal(getMedia_cheg(),getMedia_serv(),tipo)[0],s,this));
            }
            else if(tipo == 4){
                s.insereEvento(new Saida(s.getInstante() + Aleatorio.normal(getMedia_cheg(),getMedia_serv(),tipo)[0],s,this));
            }
        } else {
            fila.addElement(c); // Se servico ocupado, o cliente vai para a fila de espera
        }
    }

    // Metodo que remove cliente do servico
    public void removeServico() {
        atendidos++; // Regista que acabou de atender + 1 cliente
        if (fila.size() == 0) {
            estado--; // Se a fila esta vazia, liberta o servico
        } else { // Se nao,
            // vai buscar proximo cliente a fila de espera e
            Cliente c = (Cliente)fila.firstElement();
            fila.removeElementAt(0);
            if (tipo == 1){
                s.insereEvento(new TransServicos(s.getInstante() + Aleatorio.normal(getMedia_cheg(),getMedia_serv(),tipo)[0],s,c,this));
            }
            else if (tipo == 2){
                s.insereEvento(new TransServicos(s.getInstante() + Aleatorio.normal(getMedia_cheg(),getMedia_serv(),tipo)[0],s,c,this));
            }
            else if (tipo == 3)
                s.insereEvento(new Saida(s.getInstante() + Aleatorio.normal(getMedia_cheg(),getMedia_serv(),tipo)[0],s,this));
            else if(tipo ==4){
                s.insereEvento(new Saida(s.getInstante() + Aleatorio.normal(getMedia_cheg(),getMedia_serv(),tipo)[0],s,this));
            }
            // agenda a sua saida para daqui a s.getMedia_serv() instantes
        }
    }

    // Metodo que calcula valores para estatasticas, em cada passo da simulacao ou evento
    public void act_stats() {
        // Calcula tempo que passou desde o ultimo evento
        double temp_desde_ult = s.getInstante() - temp_ult;
        // Actualiza variavel para o proximo passo/evento
        temp_ult = s.getInstante();
		// Contabiliza tempo de espera na fila
        // para todos os clientes que estiveram na fila durante o intervalo
        soma_temp_esp += fila.size() * temp_desde_ult;
        // Contabiliza tempo de atendimento
        soma_temp_serv += estado * temp_desde_ult;
    }

    // Metodo que calcula valores finais estataticos
    public String relat() {
        String relatorio = new String();
        // Tempo m�dio de espera na fila
        double temp_med_fila = soma_temp_esp / (atendidos + fila.size());
		// Comprimento m�dio da fila de espera
        // s.getInstante() neste momento � o valor do tempo de simula��o,
        // uma vez que a simula��o come�ou em 0 e este m�todo s� � chamdo no fim da simula��o
        double comp_med_fila = soma_temp_esp / s.getInstante();
        // Tempo m�dio de atendimento no servi�o
        double utilizacao_serv = soma_temp_serv / s.getInstante();
        // Apresenta resultados

        System.out.println("Tempo medio de espera " + temp_med_fila);

        System.out.println("Comp. medio da fila " + comp_med_fila);
        
        System.out.println("Utilizacao do servico " + ((double)utilizacao_serv/(double)natendedores));
        
        System.out.println("Tempo de simulacao " + s.getInstante()); // Valor actual

        System.out.println("Numero de clientes atendidos " + atendidos);
        System.out.println("Numero de clientes na fila " + fila.size()); // Valor actual
        
        relatorio+="Tempo medio de espera " + temp_med_fila+"\n"+"Comp. medio da fila " + comp_med_fila+"\n"+"Utilizacao do servico " + ((double)utilizacao_serv/(double)natendedores)+"\n"+"Tempo de simulacao " + s.getInstante()+"\n"+"Numero de clientes atendidos " + atendidos+"\n"+"Numero de clientes na fila " + fila.size()+"\n";
        return relatorio;
    }

    public int getTipo() {
        return tipo;
    }

    public double getMedia_cheg() {
        return media_cheg;
    }

    public double getMedia_serv() {
        return media_serv;
    }
    
    // M�todo que devolve o n�mero de clientes atendidos no servi�o at� ao momento
    public int getAtendidos() {
        return atendidos;
    }

    public Vector<Cliente> getFila() {
        return fila;
    }
    
}
