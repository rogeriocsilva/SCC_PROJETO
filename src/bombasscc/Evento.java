/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bombasscc;

// Classe de onde v0o ser derivados todos os eventos.
// Cont0m apenas os atributos e m0todos comuns a todos os eventos.
// Por isso 0 uma classe abstracta. N0o haver0 inst0ncias desta classe num simulador.

public abstract class Evento {

	protected double instante;  // Instante de ocorr0ncia do evento
	protected Simulador s;      // Simulador onde ocorre o evento

	//Construtor
    Evento (double i, Simulador s){
		instante = i;
		this.s = s;
	}

	// M0todo que determina se o evento corrente ocorre primeiro, ou n0o, do que o evento e1
	// Se sim, devolve true; se n0o, devolve false
	// Usado para ordenar por ordem crescente de instantes de ocorr0ncia a lista de eventos do simulador
    public boolean menor (Evento e1){
		return (instante < e1.instante);
	}

	// M0todo que executa um evento; a ser definido em cada tipo de evento
    abstract void executa ();

    // M0todo que devolve o instante de ocorr0ncia do evento
    public double getInstante() {
        return instante;
    }
}