/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bombasscc;

import java.util.*;

// Classe que cont0m, em cada instante, os eventos a serem executados, ordenados por instantes de ocorr0ncia crescentes.
// Funciona como uma agenda.
// Deriva da classe LinkedList.

public class ListaEventos extends LinkedList<Evento> {

    private Simulador s;  // Simulador a que pertence a lista de eventos
    private static final long serialVersionUID = 1; // n0mero para serializa00o 
    
    // Construtor
    ListaEventos (Simulador s){
        this.s = s;
    }

    // M0todo para inserir um evento na lista de eventos
    public void insereEvento (Evento e1){
	int i = 0;
	    // Determina posi00o correcta do evento e1 na lista
        // A lista 0 ordenada por ordem crescente dos instantes de ocorr0ncia dos eventos
	    while (i < size() && ((Evento)get(i)).menor(e1)) i++;
	    // Coloca evento e1 na lista
        add(i, e1);
    }

    // M0todo informativo apenas. Imprime o conte0do da lista de eventos em cada instante
    public void print (){
    int i;
        System.out.println ("--- Lista de eventos em " + s.getInstante() + " ---");
        for (i = 0; i < size(); i++) System.out.println ("Evento " + (i+1) + " e uma " + (Evento)(get(i)));
    }
}