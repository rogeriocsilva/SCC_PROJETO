/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bombasscc;


public class Simulador {

    // Relogio de simulacao - variavel que contem o valor do tempo em cada instante
    private double instante;
    // Numero de clientes que vao ser atendidos
    private int n_clientes;
    // Servico - pode haver mais do que um num simulador
    //bomba
    private Servico bomba, bombagasoleo;
    private double bomba_m_chegada=2.5, bomba_m_serv=4;
    private double chegada = 1.2;
    //numero de atendedores bomba
    private int nbomba = 2;
    private int nbombagasoleo = 1;
    //caixa
    public Servico caixa;
    private double caixa_m_chegada=0, caixa_m_serv=1;
    //numero de atendedores caixa
    private int ncaixas = 1;
    //tempo de simulação
    private double tempo=400;
    private boolean cenario;
    // Lista de eventos - onde ficam registados todos os eventos que vao ocorrer na simulacao
    // Cada simulador so tem uma
    private ListaEventos lista;

    // Construtor
    public Simulador() {

    }



    // Metodo que insere o evento e1 na lista de eventos
    void insereEvento(Evento e1) {
        lista.insereEvento(e1);
    }

    // Metodo que actualiza os valores estataticos do simulador
    private void act_stats() {
        if(cenario){
            bomba.act_stats();
        }else{
            bomba.act_stats();
            bombagasoleo.act_stats();
            caixa.act_stats(); 
        }

    }

    // Metodo que apresenta os resultados de simulacao finais
    private String relat() {

        String relatorio = new String();
        
        if(cenario){
            relatorio+="\n------- Resultados bomba gasolina -------\n"+bomba.relat();
        }else{
            System.out.println();
            System.out.println("------- Resultados bomba gasolina -------");
            System.out.println();

            relatorio+="\n------- Resultados bomba gasolina -------\n"+bomba.relat();

            System.out.println();
            System.out.println("------- Resultados bomba gasoleo -------");
            System.out.println();

            relatorio+="\n------- Resultados bomba gasoleo -------\n"+bombagasoleo.relat();
            System.out.println("------- Resultados caixa -------");

            relatorio +="\n------- Resultados caixa -------\n"+caixa.relat();
        }
        

        
       
        return relatorio;
    }

    // Metodo executivo do simulador
    public String executa(int nbomba1, int nbombagasoleo1,int ncaixas1, int n_clientes1, double tempo1, double media1, double media2, double media3, double desvio1, double desvio2, double desvio3, boolean cenario2 ) {
        nbomba = nbomba1;
        nbombagasoleo = nbombagasoleo1;
        ncaixas = ncaixas1;
        tempo = tempo1;
        bomba_m_chegada = media1;
        bomba_m_serv = desvio1;
        caixa_m_chegada = media3;
        caixa_m_serv = desvio3;
        cenario = cenario2;
        // Inicializacao de parametros do simulador
        n_clientes = n_clientes1;
        // Inicializacao do relogio de simulacao
        instante = 0;
        
        if(!cenario){
                        // Criacao do servico
             bomba = new Servico(this, bomba_m_chegada, bomba_m_serv, instante, 1, nbomba);
             // Criacao do servico
             bombagasoleo = new Servico(this, media2, desvio2, instante, 2, nbombagasoleo);
             caixa = new Servico(this, caixa_m_chegada, caixa_m_serv, instante, 3, ncaixas);
             // Criacao da lista de eventos
             lista = new ListaEventos(this);
             // Agendamento da primeira chegada
             // Se nao for feito, o simulador nao tem eventos para simular
             insereEvento(new Chegada(instante, this, cenario));
             Evento e1;
             // Enquanto nao atender todos os clientes
             while (instante < tempo) {
                 //lista.print();  // debug
                 e1 = (Evento) (lista.removeFirst());  // Retira primeiro evento (o mais iminente) da lista de eventos
                 instante = e1.getInstante();         // Actualiza relogio de simulacao
                 e1.executa();                       // Executa evento
                 act_stats();                         // Actualiza valores estatasticos, editar para os varios servicos

             }
             return relat();  // Apresenta resultados de simulacao finais 
        }
        else{
            bomba = new Servico(this, bomba_m_chegada, bomba_m_serv, instante, 4, 4);
            lista = new ListaEventos(this);
            insereEvento(new Chegada(instante, this, cenario));
            Evento e1;
             // Enquanto nao atender todos os clientes
             while (instante < tempo) {
                 //lista.print();  // debug
                 e1 = (Evento) (lista.removeFirst());  // Retira primeiro evento (o mais iminente) da lista de eventos
                 instante = e1.getInstante();         // Actualiza relogio de simulacao
                 e1.executa();                       // Executa evento
                 act_stats();                         // Actualiza valores estatasticos, editar para os varios servicos

             }
             return relat();  // Apresenta resultados de simulacao finais 
        }

    }

    // Metodo que devolve o instante de simulacao corrente
    public double getInstante() {
        return instante;
    }

    public Servico getCaixa() {
        return caixa;
    }

    public Servico getBomba() {
        return bomba;
    }

    public ListaEventos getLista() {
        return lista;
    }

    public Servico getBombagasoleo() {
        return bombagasoleo;
    }

    public int getNbombagasoleo() {
        return nbombagasoleo;
    }

    public double getBomba_m_chegada() {
        return bomba_m_chegada;
    }
    public double getChegada() {
        return chegada;
    }
    
}
