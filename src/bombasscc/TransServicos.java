/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bombasscc;


public class TransServicos extends Evento {
    Servico serv;
    Cliente c;

    public TransServicos(double i, Simulador s,Cliente c,Servico serv) {
        super(i, s);
        this.c=c;
        this.serv=serv;
    }
    
    //tipo do servico, 1 para bomba e 2 para caixa
    
    void executa() {
        serv.removeServico();
        s.getCaixa().insereServico(this.c);
    }
}
