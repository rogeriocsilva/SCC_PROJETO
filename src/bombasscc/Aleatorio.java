/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bombasscc;


// Classe para geracao de numeros aleatorios segundos varias distribuicoes
// Apenas a distribuicao exponencial negativa esta definida



public class Aleatorio {
	
	static boolean flag1 = false;
        static boolean flag2 = false;
        static boolean flag3 = false;
        static boolean flag4 = false;
	static double [] valores = new double[4];
    // Gera um numero segundo uma distribuicao exponencial negativa de media m
    static double exponencial(double m) {
        return (-m * Math.log(RandomGenerator.rand64(1)));
    }

    
    static double [] normal(double media, double devpad, int tipo) {
        double [] x = new double [2];
        if(tipo == 1){
            if(flag1){
                flag1 = false;
                x[0] = valores[0];
                return x;
            }

            double v1, v2, w, y1, y2,
            temp, u1, u2;

            do{
            do {
                u1 = RandomGenerator.rand64(10);
                u2 = RandomGenerator.rand64(10);
                v1 = 2 * u1 - 1;
                v2 = 2 * u2 - 1;
                w = v1 * v1 + v2 * v2;
            } while (w > 1);

            temp = Math.pow(((-2) * Math.log(w) / w), 0.5);
            y1 = v1 * temp;
            y2 = v2 * temp;

            x[0] = media + y1 * devpad;
            x[1] = media + y2 * devpad;
            }while(x[0]<0 || x[1]<0);

            valores[0] = x[1];
            flag1 = true;
            return x;
        }
        else if(tipo == 2){
            if(flag2){
                flag2 = false;
                x[0] = valores[1];
                return x;
            }

            double v1, v2, w, y1, y2,
            temp, u1, u2;

            do{
            do {
                u1 = RandomGenerator.rand64(10);
                u2 = RandomGenerator.rand64(10);
                v1 = 2 * u1 - 1;
                v2 = 2 * u2 - 1;
                w = v1 * v1 + v2 * v2;
            } while (w > 1);

            temp = Math.pow(((-2) * Math.log(w) / w), 0.5);
            y1 = v1 * temp;
            y2 = v2 * temp;

            x[0] = media + y1 * devpad;
            x[1] = media + y2 * devpad;
            }while(x[0]<0 || x[1]<0);

            valores[1] = x[1];
            flag2 = true;
            return x;
        }
        else if(tipo == 3){
            if(flag3){
                flag3 = false;
                x[0] = valores[2];
                return x;
            }

            double v1, v2, w, y1, y2,
            temp, u1, u2;

            do{
            do {
                u1 = RandomGenerator.rand64(10);
                u2 = RandomGenerator.rand64(10);
                v1 = 2 * u1 - 1;
                v2 = 2 * u2 - 1;
                w = v1 * v1 + v2 * v2;
            } while (w > 1);

            temp = Math.pow(((-2) * Math.log(w) / w), 0.5);
            y1 = v1 * temp;
            y2 = v2 * temp;

            x[0] = media + y1 * devpad;
            x[1] = media + y2 * devpad;
            }while(x[0]<0 || x[1]<0);

            valores[2] = x[1];
            flag3 = true;
            return x;
        }
        else if(tipo == 4){
            if(flag4){
                flag4 = false;
                x[0] = valores[3];
                return x;
            }

            double v1, v2, w, y1, y2,
            temp, u1, u2;

            do{
            do {
                u1 = RandomGenerator.rand64(10);
                u2 = RandomGenerator.rand64(10);
                v1 = 2 * u1 - 1;
                v2 = 2 * u2 - 1;
                w = v1 * v1 + v2 * v2;
            } while (w > 1);

            temp = Math.pow(((-2) * Math.log(w) / w), 0.5);
            y1 = v1 * temp;
            y2 = v2 * temp;

            x[0] = media + y1 * devpad;
            x[1] = media + y2 * devpad;
            }while(x[0]<0 || x[1]<0);

            valores[3] = x[1];
            flag4 = true;
            return x;
        }
     	return null;

    }
}
