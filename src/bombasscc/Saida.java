/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bombasscc;

// Classe que representa a sa0da de um cliente. Deriva de Evento.

public class Saida extends Evento {
    Servico serv;
	//Construtor
	Saida (double i, Simulador s, Servico serv1){
		super(i, s);
                serv = serv1;
	}

	// M0todo que executa as ac00es correspondentes 0 sa0da de um cliente
	
	void executa(){
		// Retira cliente do servi0o
        serv.removeServico();
    }

    // M0todo que descreve o evento.
    // Para ser usado na listagem da lista de eventos.
    public String toString(){
         return "Sada em " + instante;
    }


}