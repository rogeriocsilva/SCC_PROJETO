/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bombasscc;


// Classe que representa a chegada de um cliente. Deriva de Evento.
public class Chegada extends Evento {
    boolean cenario;
    //Construtor
    Chegada(double i, Simulador s, boolean cenario2) {
        super(i, s);
        cenario = cenario2;
    }

    // M�todo que executa as ac��es correspondentes � chegada de um cliente
    
    void executa() {
        
        if(!cenario){
                        // Coloca cliente no servi�o - na fila ou a ser atendido, conforme o caso
            if(RandomGenerator.rand64(2)<0.20){
                s.getBombagasoleo().insereServico(new Cliente(2));
            }
            else{
                s.getBomba().insereServico(new Cliente(1));
            }

            // Agenda nova chegada para daqui a Aleatorio.exponencial(s.media_cheg) instantes
            s.insereEvento(new Chegada(s.getInstante() + Aleatorio.exponencial(s.getChegada()), s, cenario));
        }
        else{
            s.getBomba().insereServico(new Cliente(1));
            s.insereEvento(new Chegada(s.getInstante() + Aleatorio.exponencial(s.getChegada()), s, cenario));
        }

    }

    // M�todo que descreve o evento.
    // Para ser usado na listagem da lista de eventos.
    public String toString() {
        return "Chegada em " + instante;
    }
}
